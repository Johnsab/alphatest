Assumptions:
1. Stream data:
	The data is streamed from the website, using yield to operate on each line of text as it arrives.
	
2. Word delimiter is space
	The words in the file are delimited by space.
	
3. All words are valid.

4. There is an exclusion list that can be amended over time.

5. Include a test that split works correctly.
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;

namespace WordCounter
{
    class Program
    {
        static void Main(string[] args)
        {
            IDictionary<string, int> result = new Dictionary<string, int>();
            Regex rgx = new Regex("[^a-zA-Z0-9 ]");
            List<string> words = new List<string> { "href", "archive", "ariahiddentrue", "mediaType", "clicktracking" };

            var wordCounter = new ParallelWordCounter();

            foreach (var item in func())
            {
                string str = rgx.Replace(item.ToString(), "");
                var charArray = item.ToString().ToCharArray();
                int pos = Array.IndexOf(charArray, '<');

                var exclude = words.Any(w => str.Contains(w));

                if (!exclude)
                {
                    if (pos == -1 && !string.IsNullOrWhiteSpace(str))
                    {
                        var returnedWords = wordCounter.CountWords(str);
                        BuildList(result, returnedWords);

                    }
                }
            }

            Console.WriteLine("The top ten most frequently occuring words are:");
            Console.WriteLine();
            Console.WriteLine("________________________");
            Console.WriteLine("Word \t Frequency");
            Console.WriteLine("________________________");
            Console.WriteLine();

            var topTen = result.Values.ToList().OrderByDescending(i => i).Take(10);

            foreach (var value in topTen)
            {
                var myKey = result.FirstOrDefault(x => x.Value == value).Key;

                Console.WriteLine($"{myKey}\t{result[myKey]}");
            }

            Console.ReadLine();

        }

        private static void BuildList(IDictionary<string, int> result, IDictionary<string, int> returnedWords)
        {
            foreach (var word in returnedWords.Keys)
            {
                if (result.ContainsKey(word))
                {
                    int tempValue = 0;
                    result.TryGetValue(word, out tempValue);
                    tempValue = tempValue + returnedWords[word];
                    result.Remove(word);
                    result.Add(word, tempValue);
                }
                //if result contains key add values
                else
                {
                    result.TryAdd(word, returnedWords[word]);
                }
            }
        }

        public static IEnumerable func()
        {
            WebClient myWebClient = new WebClient();

            Console.WriteLine("Accessing {0} ...", "https://archive.org/stream/TheLordOfTheRing1TheFellowshipOfTheRing/The+Lord+Of+The+Ring+1-The+Fellowship+Of+The+Ring_djvu.txt");

            Stream myStream = myWebClient.OpenRead("https://archive.org/stream/TheLordOfTheRing1TheFellowshipOfTheRing/The+Lord+Of+The+Ring+1-The+Fellowship+Of+The+Ring_djvu.txt");

            StreamReader sr = new StreamReader(myStream);

            while (!sr.EndOfStream)
            {
                yield return sr.ReadLine();
            }

            myStream.Close();
        }
    }
}

﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace WordCounter
{
    public class ParallelWordCounter : IWordCount
    {
        private NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        public IDictionary<string, int> CountWords(string textLine)
        {
            var result = new Dictionary<string, int>();

            var words = textLine.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);


            //Loop through words add to dictionary
            try
            {
                foreach (var word in words)
                {
                    if (!result.ContainsKey(word))
                    {
                        result.Add(word, 1);
                    }
                    else
                    {
                        int tempValue = 0;
                        result.TryGetValue(word, out tempValue);
                        tempValue++;
                        result.Remove(word);
                        result.Add(word, tempValue);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Could not loop through words");
            }

            return result;
        }
    }
}

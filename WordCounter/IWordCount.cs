﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WordCounter
{
    interface IWordCount
    {
        IDictionary<string, int> CountWords(string path);
    }
}

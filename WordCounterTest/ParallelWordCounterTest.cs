using NUnit.Framework;
using WordCounter;

namespace Tests
{
    public class ParallelWordCounterTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void ParallelWordCounterShouldReturn5Words()
        {
            //Arrange
            string textLine = "The quick brown fox jumped";
            var wordCounter = new ParallelWordCounter();

            //Act
            var wordList = wordCounter.CountWords(textLine);

            //Assert
            Assert.AreEqual(5, wordList.Keys.Count);
        }

        
    }
}